import Glass from "../models/glass.js";
import ListGlass from "../models/listGlass.js";
let listGlass = new ListGlass();
let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];
for (let i in dataGlasses) {
  let newGlass = new Glass(
    dataGlasses[i].id,
    dataGlasses[i].src,
    dataGlasses[i].virtualImg,
    dataGlasses[i].brand,
    dataGlasses[i].name,
    dataGlasses[i].color,
    dataGlasses[i].price,
    dataGlasses[i].description
  );
  listGlass.addGlass(newGlass);
}
console.log(listGlass);

const renderListGlass = (data) => {
  let listGlassHTML = "";
  for (let i in data) {
    listGlassHTML += `
        <div onclick="handleShowInfo('${data[i].id}')" class="col-3 p-3"   >
            <img src="${data[i].src}" alt="" style="max-width: 100%" />
        </div>
        `;
  }
  document.getElementById("vglassesList").innerHTML = listGlassHTML;
};
const handleShowInfo = (id) => {
  console.log(id);
  for (let i in listGlass.arr) {
    // console.log(listGlass.arr[i].id);
    if (listGlass.arr[i].id === id) {
      console.log(i);
      document.getElementById("avatar").innerHTML = `
                 <img id="avatarGlass" src="${listGlass.arr[i].virtualImg}" alt="" style="max-width: 100%" />
            `;
      document.getElementById("glassesInfo").style.display = "block";
      document.getElementById("glassesInfo").innerHTML = `
        <div class="row">
          <div class="col-6">
              <p class="m-0"><span style="font-size:1.3rem">Brand:</span> ${listGlass.arr[i].brand}</p>
          </div>
          <div class="col-6">
              <p class="m-0"><span style="font-size:1.3rem">Name:</span> ${listGlass.arr[i].name}</p>
          </div>
          <div class="col-6">
              <p class="m-0"><span style="font-size:1.3rem">Color:</span> ${listGlass.arr[i].color}</p>
          </div>
          <div class="col-6">
              <p class="m-0"><span style="font-size:1.3rem">Price:</span> ${listGlass.arr[i].price}$</p>
          </div>
          <div class="col-12">
              <p class="m-0"><span style="font-size:1.3rem">Description:</span> ${listGlass.arr[i].description}</p>
          </div>
        </div>
            `;
      i = +i;
      let j = i - 1;
      let h = i + 1;
      if (j === -1) j++;
      if (h === listGlass.arr.length) h--;
      document.getElementById("btn").innerHTML = `
            <button class="btn btn-warning" onclick="handleShowInfo('${
              listGlass.arr[String(j)].id
            }')">
                Before
            </button>
            <button class="btn btn-warning" onclick="handleShowInfo('${
              listGlass.arr[String(h)].id
            }')">
                After
            </button>
            `;
    }
  }
};
window.handleShowInfo = handleShowInfo;
const removeGlasses = (choose) => {
  console.log(1);
  if (choose == false)
    document.getElementById("avatarGlass").style.display = "none";
  else {
    document.getElementById("avatarGlass").style.display = "block";
  }
};
window.removeGlasses = removeGlasses;
renderListGlass(listGlass.arr);
